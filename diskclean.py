#!/usr/bin/python3
""" diskclean.py
Clean up a filesystem from files that haven't been touched in the last XX days.
Example:
[StartDir]
 |
 +---UntouchedFile
 +--UntouchedOne
 |  |
 |  +--Checked
 |  +--Checked2
 |     |
 |     +---File1
 |     +---File2
 |
 +--UntouchedTwo
 +--UntouchedThree
    |
    +--Checked3
       |
       +--Checked4
 
- UntouchedOne to UntouchedThree will not be checked, these remain unharmed
- UntouchedFile will not be checked, these remains unharmed
- Checked to Checked4 will be checked and removed, if older than [TimeToKeep] days AND empty
- File1 to File2 will be checked and removed if older than [TimeToKeep] days
Created:    Mathias Menzer, 2015 , EUPL 1.2 or later
Modified:   Mathias Menzer, 2019-11-17, Directroy/File Check;
Modified:   Mathias Menzer, 2020-06-02, Statistics
"""

import os
import time
from datetime import datetime

StartDir = '/home/mathias/testing'  # The starting Directory
TimeToKeep = 20                     # Delete Files older that XX days
nowTime = time.time()               # actual time
# read the list of Directories that are to be emptied
baseDirs = os.listdir(StartDir)
countRemoved = 0
countSkipped = 0
countUnknown = 0
countTotal = 0


def findobjects(StartDir):
    # Resursively list all files & folders with path
    ObjectList = []
    for root, dirs, files in os.walk(StartDir):
        for name in files:
            ObjectList.append(root + '/' + name)
        for name in dirs:
            ObjectList.append(root + '/' + name)
    return ObjectList


print('Remove everything before ' + str(datetime.utcfromtimestamp(nowTime -
      TimeToKeep * 86400).strftime('%Y-%m-%d')))
for item in baseDirs:
    # Loop through all the base Directories
    for thing in findobjects(StartDir + '/' + item):
        #loop through all Files and Directories found below the base Directroies
        countTotal += 1
        if os.stat(thing).st_mtime < nowTime - TimeToKeep * 86400:
            # Is the Object older that [TimeToKeep] days?
            if os.path.isfile(thing):
                # Is the Object a file?
                print('  Removed   ' + str(datetime.utcfromtimestamp(
                    os.stat(thing).st_mtime).strftime('%Y-%m-%d')) + ' ['+thing+']')
                os.remove(thing)
                countRemoved += 1
            elif os.path.isdir(thing):
                # Is the Object a Directory?
                if len(os.listdir(thing)) == 0:
                    print('  Removed   ' + str(datetime.utcfromtimestamp(
                        os.stat(thing).st_mtime).strftime('%Y-%m-%d')) + ' ['+thing+']')
                    os.rmdir(thing)
                    countRemoved += 1
                else:
                    print('  Not Empty ' + str(datetime.utcfromtimestamp(
                        os.stat(thing).st_mtime).strftime('%Y-%m-%d')) + ' ['+thing+']')
                    countSkipped += 1
            else:
                # Is the Object neither a File nor a Directory?
                print('  Unknown   ' + str(datetime.utcfromtimestamp(
                    os.stat(thing).st_mtime).strftime('%Y-%m-%d')) + ' ['+thing+']')
                countUnknown += 1
        else:
            # Not old enough
            print('  Untouched ' + str(datetime.utcfromtimestamp(
                os.stat(thing).st_mtime).strftime('%Y-%m-%d')) + ' ['+thing+']')
            countSkipped += 1
print('')
print('Remove everything older than ' + str(TimeToKeep) + ' days, before ' +
      str(datetime.utcfromtimestamp(nowTime - TimeToKeep * 86400).strftime('%Y-%m-%d')))
print('  Removed: ' + str(countRemoved))
print('  Skipped: ' + str(countSkipped))
print('  Unknown: ' + str(countUnknown))
print('  -------------------')
print('  Total  : ' + str(countTotal))