#!/bin/bash
# Upgrade Raspberry Pi from Buster to Bullseye

sudo apt update
sudo apt upgrade -y
sudo apt full-upgrade -y
sudo sed -i /deb/s/buster/bullseye/g /etc/apt/sources.list.d/raspi.list
sudo sed -i /deb/s/buster/bullseye/g /etc/apt/sources.list
sudo apt remove apt-listchanges -y
sudo apt update
sudo apt upgrade -y
sudo apt full-upgrade -y
sudo apt install apt-listchanges -y
sudo apt autoremove -y
sudo apt autoclean -y
sudo reboot