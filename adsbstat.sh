#!/bin/bash
echo ''
echo '### FlightAware #################################'
piaware-status
echo ''
echo '### Flightradar24 ###############################'
fr24feed-status
echo ''
echo '### Opensky Network #############################'
systemctl status opensky-feeder.service
