#!/usr/bin/env bash
# Check if local Router can be reached, if not restart Network Service
# --> Add to Crontab, running as root

host="10.0.0.1"              # This should be the router in the local subnet
response="Unreachable"
result=`ping -c1 $host 2>&1 | grep $response`
#echo $result

if [ ! "$result" = "" ]; then
        echo "It's down!! Attempting to restart."
        service network restart
fi