#!/usr/bin/env python3
"""
    Name:    convert_subnet_list.py
    Descr:   Converts the CIDR on the first Column (row[0]) of a CSV to an IP Range
    Ver:     21.12.31
    Author:  Mathias Menzer
    License: European Union Public License (EUPL) 1.2 or later
    Licence: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
    Change:  created Script
             added read from SourceCsv
             added write to TargetCsv
             added query for Arguments
"""

# Import Modules ####################################################
import sys, ipaddress, csv


# Definitions #######################################################
SourceCsv = '/home/mathias/Projekte/Skripte/Python/subnet_list.csv'
TargetCsv = '/home/mathias/Projekte/Skripte/Python/range_list.csv'


# Convert CIDR to Range #############################################
def convert_cidr(myCIDR):
    MyNet = ipaddress.ip_network(myCIDR)
    MyRange = '%s-%s' % (MyNet[0], MyNet[-1])
    return(MyRange)


# Main Function #####################################################
def main(args):
    if len(args) > 0:
        # print("There are Arguments: " + str(args))
        for arg in args:
            if arg in ("-i", "-inputfile", "--inputfile"):
                InputFile = args[args.index(arg)+1]
            elif arg in ("-o", "-outputfile", "--outputfile"):
                OutputFile= args[args.index(arg)+1]
    else:
        InputFile = SourceCsv
        OutputFile = TargetCsv

    with open(InputFile) as InFile:
        InCsv = csv.reader(InFile)
        with open(OutputFile, 'w') as OutFile:
            OutCsv = csv.writer(OutFile, delimiter=',', quotechar='"')
            for row in InCsv:
                if row[0] == "CIDR":
                    OutCsv.writerow(['CIDR','Range'])
                    continue
                MyRange = convert_cidr(row[0])
                print(row[0] +" -  " + MyRange)
                row[0] = MyRange
                OutCsv.writerow(row)


#####################################################################
# Call Main Function
#####################################################################
if __name__ == "__main__":
    main(sys.argv[1:])
