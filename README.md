# any random Scripts

![MyScripts](https://badgen.net/badge/MyScripts/Experimental/red?icon=git)

The scripts and Snippets in this Repo should be considered Experimental. So don't blame me, if they messed up your something!

- `adsbstat.sh:` Shell Script to get Info of all ADSB Feeder Services [Raspberry with ADSB Receiver]
- `convert_subnet_list.py:` Convert the first (or any other) in a given CSV from CIDR to IP Range Notation
- `diskclean.py:` Python Script to find and remove Files that have not been touche in a specified Number of Days.
- `upgrade.sh:` Shell Script to automate the Upgrade from one Raspbian Main Version to the next one !!Use with Care!!
- `watchdog.sh:` Shell Script for crontab to restart the local Network Services if the local Router cannot be reached anymore

# License

## EUPL 
Unless marked otherwise, all Scripts in this repository are licensed under the European Union Public Licence (EUPL) 1.2 or later

<!---
[![EUPL](https://github.com/eClip-/EUPL-badge/blob/master/eupl_common.svg)](https://joinup.ec.europa.eu/collection/eupl)
-->

### Links:
- EUPL Text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
- EUPL Overview at EC Joinup: https://joinup.ec.europa.eu/collection/eupl
- EUPL at the European Comission Website: https://ec.europa.eu/info/european-union-public-licence_en

## WTFPL
Some Scripts might be under WTFPL (Do What The Fuck You Want To Public License)

<!---
[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-2.png)](http://www.wtfpl.net)
-->

### Links: 
- WTFPL: http://www.wtfpl.net/txt/copying
- FAQ: http://www.wtfpl.net/faq/
